<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $quantity = $faker->numberBetween(1, 10);
    $price = $faker->numberBetween(100, 10000);
    $discount = $faker->numberBetween(1, 100);

    return [
        'product_id' => factory(\App\Product::class)->create(),
//        'order_id' => factory(\App\Order::class, 2)->create(),
        'quantity' => $quantity,
        'price' => $price,
        'discount' => $discount,
        'sum' => round($price * ($discount / 100) * $quantity),
    ];
});
