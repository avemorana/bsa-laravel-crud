<?php

use Illuminate\Database\Seeder;

class OrderItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Order::class, 10)->create()->each(function ($order) {
            $order->orderItems()->saveMany(factory(\App\OrderItem::class, 2)->make(['order_id' => null]));
        });
    }
}
