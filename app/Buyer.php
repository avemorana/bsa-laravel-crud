<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table = 'buyers';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = ['name', 'surname', 'country', 'city', 'addressLine'];

    public function orders()
    {
        return $this->hasMany(Order::class, 'buyer_id', 'id');
    }


}
