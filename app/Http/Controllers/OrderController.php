<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();

        return $orders->map(function ($order) {
            return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();
        $order = Order::create([
            'date' => date('Y-m-d H:i:s'),
            'buyer_id' => $data['buyerId']
        ]);
        $order->save();

        $orderItems = $data['orderItems'];
        foreach ($orderItems as $orderItem) {
            $product = Product::find($orderItem['productId']);
            $orderItemRow = OrderItem::create([
                'product_id' => $product->id,
                'order_id' => $order->id,
                'quantity' => $orderItem['productQty'],
                'price' => $product->price,
                'discount' => $orderItem['productDiscount'] * 100,
                'sum' => round($orderItem['productQty'] * $product->price * $orderItem['productDiscount'])
            ]);
            $orderItemRow->save();
        }
        return new Response($order);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new OrderResource(Order::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        if (!$order) return new Response([
            'result' => 'fail',
            'message' => 'order not found'
        ]);

        $data = Input::all();
        $orderItems = $data['orderItems'];
        $orderId = $data['orderId'];
        if ($id != $orderId) {
            return new Response([
                'result' => 'fail',
                'message' => 'id`s error'
            ]);
        }
        $result = OrderItem::where('order_id', $orderId)->delete();

        foreach ($orderItems as $orderItem) {
            $product = Product::find($orderItem['productId']);
            $orderItemRow = OrderItem::create([
                'product_id' => $product->id,
                'order_id' => $order->id,
                'quantity' => $orderItem['productQty'],
                'price' => $product->price,
                'discount' => $orderItem['productDiscount'] * 100,
                'sum' => round($orderItem['productQty'] * $product->price * $orderItem['productDiscount'])
            ]);
            $orderItemRow->save();
        }
        return new Response($order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OrderItem::where('order_id', $id)->delete();
        $result = Order::find($id)->delete();
        return ['result' => $result ? 'success' : 'fail'];
    }
}
