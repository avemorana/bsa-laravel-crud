<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $orderItems = $this->orderItems->map(function ($orderItem) {
            return [
                'productName' => $orderItem->product->name,
                'productQty' => $orderItem->quantity,
                'productPrice' => $orderItem->price,
                'productDiscount' => $orderItem->discount / 100,
                'productSum' => $orderItem->sum
            ];

        });

        $sum = 0;
        foreach ($this->orderItems as $orderItem){
            $sum += $orderItem->sum;
        }
        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $sum,
            'orderItems' => $orderItems,
            'buyer' => [
                'buyerFullName' => $this->buyer->name . ' ' . $this->buyer->surname,
                'buyerAddress' => $this->buyer->country . ' ' . $this->buyer->city . ' ' . $this->buyer->addressLine,
                'buyerPhone' => $this->buyer->phone
            ]
        ];
    }
}
